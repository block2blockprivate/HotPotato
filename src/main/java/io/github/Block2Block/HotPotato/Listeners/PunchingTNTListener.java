package io.github.Block2Block.HotPotato.Listeners;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class PunchingTNTListener implements Listener {

    @EventHandler
    public void onPunchTNT(EntityDamageByEntityEvent e) {
        if (e.getEntityType() == EntityType.PRIMED_TNT) {
            e.getEntity().setVelocity(e.getDamager().getLocation().getDirection().setY(0.5).normalize().multiply(1.3));
        }

    }
}
