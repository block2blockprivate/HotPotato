package io.github.Block2Block.HotPotato.Listeners;

import io.github.Block2Block.HotPotato.Main;
import io.github.Block2Block.HotPotato.Managers.GlobalStuff;
import io.github.Block2Block.HotPotato.Managers.ScoreboardManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class LeaveListener implements Listener {

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        event.setQuitMessage(Main.c(null,"&7[&c-&7] &r") + event.getPlayer().getName());
        switch (ScoreboardManager.getTeamsMap().get(event.getPlayer())) {
            case "red":
                GlobalStuff.team.removePlayer(event.getPlayer());
            case "blue":
                GlobalStuff.team2.removePlayer(event.getPlayer());
        }
        ScoreboardManager.getMap().remove(event.getPlayer());
        ScoreboardManager.getScoreboardMap().remove(event.getPlayer());
        ScoreboardManager.changeLineGlobal(11, Main.c(null, "&r" + Integer.toString(Bukkit.getOnlinePlayers().size() - 1) + "/16"));
        ScoreboardManager.getKitsMap().remove(event.getPlayer());
    }
}
