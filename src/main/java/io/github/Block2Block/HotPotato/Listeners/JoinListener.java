package io.github.Block2Block.HotPotato.Listeners;

import io.github.Block2Block.HotPotato.Main;
import io.github.Block2Block.HotPotato.Managers.GlobalStuff;
import io.github.Block2Block.HotPotato.Managers.ScoreboardManager;
import io.github.Block2Block.HotPotato.entities.GameState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        if (Main.state != GameState.INGAME||Main.state != GameState.DEAD||Main.state != GameState.ENDGAME) {
            event.setJoinMessage(Main.c(null, "&7[&a+&7] &r") + event.getPlayer().getName());
            Scoreboard s = GlobalStuff.sm.getNewScoreboard();
            Objective o = s.registerNewObjective(event.getPlayer().getName(), "dummy");
            o.setDisplaySlot(DisplaySlot.SIDEBAR);
            o.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&9&m----&b&l HOTPOTATO &9&m----"));
            ScoreboardManager.getMap().put(event.getPlayer(), o);
            ScoreboardManager.getScoreboardMap().put(event.getPlayer(), s);
            ScoreboardManager.getKitsMap().put(event.getPlayer(), null);

            boolean kitChosen = false;
            String kit = "None ";
            int choose = Main.chooseRan(1, 2);
            boolean onTeam = false;
            String teamJoined = null;

            while (!onTeam) {
                switch (choose) {
                    case 1:
                        if (GlobalStuff.team.getSize() != 8) {
                            if (GlobalStuff.team2.getSize() >= GlobalStuff.team.getSize()) {
                                GlobalStuff.team.addPlayer(event.getPlayer());
                                onTeam = true;
                                event.getPlayer().setPlayerListName(ChatColor.RED + "" + event.getPlayer().getName());
                                teamJoined = "Red";
                                ScoreboardManager.getTeamsMap().put(event.getPlayer(), "red");
                                break;
                            } else {
                                choose = Main.chooseRan(1, 2);
                                break;
                            }
                        } else {
                            choose = Main.chooseRan(1, 2);
                            break;
                        }
                    case 2:
                        if (GlobalStuff.team2.getSize() != 8) {
                            if (GlobalStuff.team.getSize() >= GlobalStuff.team2.getSize()) {
                                GlobalStuff.team2.addPlayer(event.getPlayer());
                                onTeam = true;
                                event.getPlayer().setPlayerListName(ChatColor.BLUE + "" + event.getPlayer().getName());
                                teamJoined = "Blue";
                                ScoreboardManager.getTeamsMap().put(event.getPlayer(), "blue");
                                break;
                            } else {
                                choose = Main.chooseRan(1, 2);
                                break;
                            }
                        } else {
                            choose = Main.chooseRan(1, 2);
                            break;
                        }
                }
            }

            ScoreboardManager.changeLine(event.getPlayer(), 15, Main.c(null, "&3» &b&lGame"));
            ScoreboardManager.changeLine(event.getPlayer(), 14, Main.c(null, "&rHotPotato"));
            ScoreboardManager.changeLine(event.getPlayer(), 13, Main.c(null, " "));
            ScoreboardManager.changeLine(event.getPlayer(), 12, Main.c(null, "&3» &b&lPlayers"));
            ScoreboardManager.changeLineGlobal(11, Main.c(null, "&r" + Integer.toString(Bukkit.getOnlinePlayers().size()) + "/16"));
            ScoreboardManager.changeLine(event.getPlayer(), 10, Main.c(null, "  "));
            ScoreboardManager.changeLine(event.getPlayer(), 9, Main.c(null, "&3» &b&lTeam"));
            ScoreboardManager.changeLine(event.getPlayer(), 8, Main.c(null, "&r" + teamJoined));
            ScoreboardManager.changeLine(event.getPlayer(), 7, Main.c(null, "   "));
            ScoreboardManager.changeLine(event.getPlayer(), 6, Main.c(null, "&3» &b&lKit"));
            ScoreboardManager.changeLine(event.getPlayer(), 5, Main.c(null, "&r" + kit));
            ScoreboardManager.changeLine(event.getPlayer(), 4, Main.c(null, "    "));
            ScoreboardManager.changeLine(event.getPlayer(), 3, Main.c(null, "&3» &b&lMap"));
            ScoreboardManager.changeLine(event.getPlayer(), 2, Main.c(null, "&r" + GlobalStuff.mapSelected));
        } else {
            event.getPlayer().kickPlayer(Main.c("HotPotato","Sorry! The game is currently in progress or is ending!"));
        }
    }
}
