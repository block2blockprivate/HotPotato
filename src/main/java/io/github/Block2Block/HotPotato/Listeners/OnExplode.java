package io.github.Block2Block.HotPotato.Listeners;

import io.github.Block2Block.HotPotato.Main;
import io.github.Block2Block.HotPotato.Managers.GlobalStuff;
import io.github.Block2Block.HotPotato.Managers.ScoreboardManager;
import io.github.Block2Block.HotPotato.entities.GameState;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;

import static io.github.Block2Block.HotPotato.Main.endGame;

public class OnExplode implements Listener {

    @Deprecated
    @EventHandler
    public void onTntExposion(ExplosionPrimeEvent  e) {
        if (e.getEntity() instanceof TNTPrimed) {
            TNTPrimed tnt = (TNTPrimed) e.getEntity();
            Location l = e.getEntity().getLocation();
            e.setCancelled(true);
            l.getWorld().createExplosion(l, 3f, false);
            l.getWorld().playEffect(l, Effect.EXPLOSION_HUGE, 1);

            boolean gotTeam = false;
            while (!gotTeam) {
                if (l.getBlock().getType() == Material.STAINED_CLAY) {
                    if (l.getBlock().getData() == 14) {
                        GlobalStuff.livesRed--;
                        ScoreboardManager.changeLineGlobal(8, "" + GlobalStuff.livesRed);
                        endGame(0);
                        Main.state = GameState.ENDGAME;
                        break;
                    } else if (l.getBlock().getData() == 11) {
                        GlobalStuff.livesBlue--;
                        ScoreboardManager.changeLineGlobal(8, "" + GlobalStuff.livesBlue);
                        if (GlobalStuff.livesBlue == 0) {
                            Main.state = GameState.ENDGAME;
                            endGame(1);
                        }
                        break;
                    }
                    break;
                } else {
                    l.setY(l.getY()-1);
                    if (l.getY() < 0) {
                        break;
                    }
                }
            }
        }
    }

    @EventHandler
    public void onBlockExplode(BlockExplodeEvent e) {
        e.setCancelled(true);
    }
}
