package io.github.Block2Block.HotPotato.Listeners;

import io.github.Block2Block.HotPotato.Main;
import io.github.Block2Block.HotPotato.entities.GameState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodChange implements Listener {

    @EventHandler
    public void onHungerChange(FoodLevelChangeEvent e) {
        if (Main.get().state == GameState.DEAD)
            return;
        e.setFoodLevel(30);
    }
}
