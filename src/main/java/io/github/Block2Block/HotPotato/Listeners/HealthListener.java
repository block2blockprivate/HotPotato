package io.github.Block2Block.HotPotato.Listeners;

import io.github.Block2Block.HotPotato.Managers.GlobalStuff;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class HealthListener implements Listener {

    @EventHandler
    public void onHealthLose(EntityDamageEvent e) {
        if (e.getEntityType() == EntityType.PLAYER) {
            if (!GlobalStuff.isDamageArtificial) {
                e.setCancelled(true);
            } else {
                GlobalStuff.isDamageArtificial = false;
            }
        }
    }

}
