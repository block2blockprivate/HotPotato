package io.github.Block2Block.HotPotato;

import io.github.Block2Block.HotPotato.Commands.CommandForceStart;
import io.github.Block2Block.HotPotato.Commands.CommandKit;
import io.github.Block2Block.HotPotato.Listeners.*;
import io.github.Block2Block.HotPotato.Managers.GlobalStuff;
import io.github.Block2Block.HotPotato.Managers.ScoreboardManager;
import io.github.Block2Block.HotPotato.entities.GameState;
import io.github.Block2Block.HotPotato.entities.TimerEvent;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.*;
import java.util.*;

import static io.github.Block2Block.HotPotato.entities.GameState.*;

public class Main extends JavaPlugin {
    
    private static Main instance;
    
    public static GameState state = DEAD;
    
    public static int tntclock = -1;

    public static int timerLength;
    public static boolean gameStarted = false;
    public static boolean timerStarted = false;
    public static boolean spawnTnt = false;

    private static File configFile;
    private static FileConfiguration config;

    private static File mapsFile;
    private static FileConfiguration maps;

    @Override
    public void onEnable() {
        instance = this;
        getLogger().info("HotPotato has been successfully enabled!");
        registerListeners(new JoinListener(), new LeaveListener(), new BlockBreakListener(), new BlockPlaceListener(),
                new HealthListener(), new FoodChange(), new OnExplode(), new PunchingTNTListener());
        getCommand("forcestart").setExecutor(new CommandForceStart());
        getCommand("kit").setExecutor(new CommandKit());
        loadConfig();



        for (String s : getMaps().getKeys(false)) {

            Bukkit.getServer().createWorld(new WorldCreator(s));

            Bukkit.getLogger().info("Map " + s + " has been detected.");
            GlobalStuff.getWorldsMap().put(s,Bukkit.getServer().getWorld(s));
            GlobalStuff.maps.add(s);

            List<Location> lr = new ArrayList<>();
            List<Location> lb = new ArrayList<>();
            lr.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".RedSpawns.1.X"),getMaps().getInt(s + ".RedSpawns.1.Y"),getMaps().getInt(s + ".RedSpawns.1.Z")));
            lr.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".RedSpawns.2.X"),getMaps().getInt(s + ".RedSpawns.2.Y"),getMaps().getInt(s + ".RedSpawns.2.Z")));
            lr.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".RedSpawns.3.X"),getMaps().getInt(s + ".RedSpawns.3.Y"),getMaps().getInt(s + ".RedSpawns.3.Z")));
            lr.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".RedSpawns.4.X"),getMaps().getInt(s + ".RedSpawns.4.Y"),getMaps().getInt(s + ".RedSpawns.4.Z")));
            lr.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".RedSpawns.5.X"),getMaps().getInt(s + ".RedSpawns.5.Y"),getMaps().getInt(s + ".RedSpawns.5.Z")));
            lr.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".RedSpawns.6.X"),getMaps().getInt(s + ".RedSpawns.6.Y"),getMaps().getInt(s + ".RedSpawns.6.Z")));
            lr.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".RedSpawns.7.X"),getMaps().getInt(s + ".RedSpawns.7.Y"),getMaps().getInt(s + ".RedSpawns.7.Z")));
            lr.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".RedSpawns.8.X"),getMaps().getInt(s + ".RedSpawns.8.Y"),getMaps().getInt(s + ".RedSpawns.8.Z")));
            lb.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".BlueSpawns.1.X"),getMaps().getInt(s + ".BlueSpawns.1.Y"),getMaps().getInt(s + ".BlueSpawns.1.Z")));
            lb.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".BlueSpawns.2.X"),getMaps().getInt(s + ".BlueSpawns.2.Y"),getMaps().getInt(s + ".BlueSpawns.2.Z")));
            lb.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".BlueSpawns.3.X"),getMaps().getInt(s + ".BlueSpawns.3.Y"),getMaps().getInt(s + ".BlueSpawns.3.Z")));
            lb.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".BlueSpawns.4.X"),getMaps().getInt(s + ".BlueSpawns.4.Y"),getMaps().getInt(s + ".BlueSpawns.4.Z")));
            lb.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".BlueSpawns.5.X"),getMaps().getInt(s + ".BlueSpawns.5.Y"),getMaps().getInt(s + ".BlueSpawns.5.Z")));
            lb.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".BlueSpawns.6.X"),getMaps().getInt(s + ".BlueSpawns.6.Y"),getMaps().getInt(s + ".BlueSpawns.6.Z")));
            lb.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".BlueSpawns.7.X"),getMaps().getInt(s + ".BlueSpawns.7.Y"),getMaps().getInt(s + ".BlueSpawns.7.Z")));
            lb.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".BlueSpawns.8.X"),getMaps().getInt(s + ".BlueSpawns.8.Y"),getMaps().getInt(s + ".BlueSpawns.8.Z")));
            GlobalStuff.getBlueCoordsMap().put(s,lb);
            GlobalStuff.getRedCoordsMap().put(s,lr);
            List<Location> tntspawn = new ArrayList<>();
            tntspawn.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".TNTSpawns.Blue.X"),getMaps().getInt(s + ".TNTSpawns.Blue.Y"),getMaps().getInt(s + ".TNTSpawns.Blue.Z")));
            tntspawn.add(new Location(Bukkit.getServer().getWorld(s),getMaps().getInt(s + ".TNTSpawns.Red.X"),getMaps().getInt(s + ".TNTSpawns.Red.Y"),getMaps().getInt(s + ".TNTSpawns.Red.Z")));
            GlobalStuff.getTntSpawns().put(s,tntspawn);
        }
        
        state = LOBBY;
        
        
        
    }

    @Override
    public void onDisable() {
        getLogger().info("HotPotato has been successfully disabled!");
    }

    public static String c(String prefix, String message) {
        return ChatColor.translateAlternateColorCodes('&', ((prefix==null)?"&7":"&2"+prefix+">> &7") + message);
    }

    public static String cr(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static int chooseRan(int min, int max){
        Random rn = new Random();
        return rn.nextInt(max - min + 1) + min;
    }

    public static void startTimer() {
        if (Bukkit.getOnlinePlayers().size() == 16) {
            timerLength = 21;
        } else {
            timerLength = 61;
        }
        for (Player p : Bukkit.getOnlinePlayers()) {
            p.setLevel(timerLength);
            if (io.github.Block2Block.HotPotato.Managers.ScoreboardManager.getKitsMap().get(p) == null) {
                io.github.Block2Block.HotPotato.Managers.ScoreboardManager.getKitsMap().remove(p);
                io.github.Block2Block.HotPotato.Managers.ScoreboardManager.getKitsMap().put(p,"1");
                io.github.Block2Block.HotPotato.Managers.ScoreboardManager.changeLine(p,5, Main.c(null, "&rDefault"));
            }
        }
        if (GlobalStuff.mapSelected.equals("None")) {
            GlobalStuff.mapSelected = GlobalStuff.maps.get(chooseRan(0,GlobalStuff.maps.size()-1));
            ScoreboardManager.changeLineGlobal(2, Main.c(null, "&r" + GlobalStuff.mapSelected));
        }
        timerStarted = true;
        timer();
    }

    public static Timer timer2 = new Timer();


    public static void timer() {
        final Timer timer = new Timer();
        timer2 = timer;
        state = PREGAME;
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                timerLength--;
                for (Player p : Bukkit.getOnlinePlayers()) {
                    p.setLevel(timerLength);
                }
                switch (timerLength) {
                    case 60:
                    case 30:
                    case 10:
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                    case 1:
                        Bukkit.broadcastMessage(Main.c("HotPotato","The game will start in &a" + timerLength + " &7second" + (timerLength>1?"s":"") + "."));
                        for (Player p : Bukkit.getOnlinePlayers()) {

                            p.playSound(p.getLocation(), Sound.NOTE_PLING,100,1);
                        }
                        break;
                    case 0:
                        startGame();
                        timer.cancel();
                }

            }
        }, 0, 1000);
    }

    @EventHandler
    public void timer(TimerEvent te) {
        if (te.getType() != TimerEvent.TimerType.SECOND)
            return;
        if (tntclock<0)
            return;
        tntclock--;


        for (Player p : Bukkit.getOnlinePlayers()) {
            p.playSound(p.getLocation(), Sound.NOTE_STICKS, 100, 1);
        }
        switch (tntclock) {
            case 10:
            case 5:
            case 4:
            case 3:
            case 2:
            case 1:
                Bukkit.broadcastMessage(Main.c("HotPotato", "TNT will spawn in &a" + tntclock + " &7second" + (tntclock > 1 ? "s" : "") + "."));
                break;
            case 0:
                spawnTnt();
        }

    }


    public static void spawnTnt() {
        int side = chooseRan(1,2);
        if (side == 1) {
            Location l = GlobalStuff.getTntSpawns().get(GlobalStuff.mapSelected).get(0);
            World w = Bukkit.getServer().getWorld(GlobalStuff.mapSelected);
            int i = chooseRan(400,800);
            TNTPrimed TNT;
            TNT = (TNTPrimed) w.spawnEntity(l, EntityType.PRIMED_TNT);
            TNT.setFuseTicks(i);
        } else {
            Location l = GlobalStuff.getTntSpawns().get(GlobalStuff.mapSelected).get(1);
            World w = Bukkit.getServer().getWorld(GlobalStuff.mapSelected);
            int i = chooseRan(400,800);
            TNTPrimed TNT;
            TNT = (TNTPrimed) w.spawnEntity(l, EntityType.PRIMED_TNT);
            TNT.setFuseTicks(i);
        }
    }

    public static void startGame() {
        Bukkit.broadcastMessage("Game started.");
        timerStarted = false;
        gameStarted = true;
        List<String> teleportedred = new ArrayList<>();
        List<String> teleportedblue = new ArrayList<>();
        for (Location l : GlobalStuff.getRedCoordsMap().get(GlobalStuff.mapSelected)) {
            for (String s : GlobalStuff.team.getEntries()) {
                if (!teleportedred.contains(s)) {
                    teleportedred.add(s);
                    Bukkit.getPlayer(s).teleport(l);
                    break;
                }
            }
        }
        for (Location l : GlobalStuff.getBlueCoordsMap().get(GlobalStuff.mapSelected)) {
            for (String s : GlobalStuff.team2.getEntries()) {
                if (!teleportedblue.contains(s)) {
                    teleportedblue.add(s);
                    Bukkit.getPlayer(s).teleport(l);
                    break;
                }
            }
        }
        GlobalStuff.livesBlue = 3;
        GlobalStuff.livesRed = 3;
        ScoreboardManager.changeLineGlobal(9,Main.c(null,"&3» &b&lRed Lives"));
        ScoreboardManager.changeLineGlobal(8, "" + GlobalStuff.livesRed);
        ScoreboardManager.changeLineGlobal(6,"&3» &b&lBlue Lives");
        ScoreboardManager.changeLineGlobal(5, "" + GlobalStuff.livesBlue);
        tntclock = 11;
        state = INGAME;
    }

    public static void endGame(final int winner) {
        state = ENDGAME;
        if (winner == 0) {
            Bukkit.getServer().broadcastMessage(Main.c("HotPotato","Blue has won the game!"));
        } else {
            Bukkit.getServer().broadcastMessage(Main.c("HotPotato","Red has won the game!"));
        }
        Bukkit.getServer().broadcastMessage(Main.c("HotPotato","Returning to the lobby in 10 seconds..."));
        final Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            int i = 10;
            int i2 = winner;
            public void run() {
                if (i2 == 0) {
                    for (String s : GlobalStuff.team2.getEntries()) {
                        Firework e = (Firework) Bukkit.getServer().getWorld(GlobalStuff.mapSelected).spawnEntity(Bukkit.getPlayer(s).getLocation(),EntityType.FIREWORK);
                        FireworkMeta fm = e.getFireworkMeta();
                        fm.addEffect(FireworkEffect.builder().with(FireworkEffect.Type.BALL).withColor(Color.BLUE).build());
                        fm.setPower(1);
                        e.setFireworkMeta(fm);
                    }
                } else {
                    for (String s : GlobalStuff.team.getEntries()) {
                        Firework e = (Firework) Bukkit.getServer().getWorld(GlobalStuff.mapSelected).spawnEntity(Bukkit.getPlayer(s).getLocation(),EntityType.FIREWORK);
                        FireworkMeta fm = e.getFireworkMeta();
                        fm.addEffect(FireworkEffect.builder().with(FireworkEffect.Type.BALL).withColor(Color.RED).build());
                        fm.setPower(1);
                        e.setFireworkMeta(fm);
                    }
                }
                i--;
                switch (i) {
                    case 0:
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            p.teleport(new Location(Bukkit.getServer().getWorld("Spawn"),33.5,189,-38.5));
                        }
                        state = GameState.LOBBY;
                        GlobalStuff.mapSelected = "None";
                        ScoreboardManager.getTeamsMap().clear();
                        for (String s : GlobalStuff.team.getEntries()) {
                            GlobalStuff.team.removeEntry(s);
                        }
                        for (String s : GlobalStuff.team2.getEntries()) {
                            GlobalStuff.team2.removeEntry(s);
                        }
                        resetScoreboard();
                        if (Bukkit.getOnlinePlayers().size() >= 8) {
                            startTimer();
                        }
                        timer.cancel();
                }

            }
        }, 0, 1000);
    }

    private static void resetScoreboard() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            int choose = chooseRan(1, 2);
            boolean onTeam = false;
            String teamJoined = null;

            while (!onTeam) {
                switch (choose) {
                    case 1:
                        if (GlobalStuff.team.getSize() != 8) {
                            if (GlobalStuff.team2.getSize() >= GlobalStuff.team.getSize()) {
                                GlobalStuff.team.addPlayer(p);
                                onTeam = true;
                                p.setPlayerListName(org.bukkit.ChatColor.RED + "" + p.getName());
                                teamJoined = "Red";
                                ScoreboardManager.getTeamsMap().put(p, "red");
                                break;
                            } else {
                                choose = chooseRan(1, 2);
                                break;
                            }
                        } else {
                            choose = chooseRan(1, 2);
                            break;
                        }
                    case 2:
                        if (GlobalStuff.team2.getSize() != 8) {
                            if (GlobalStuff.team.getSize() >= GlobalStuff.team2.getSize()) {
                                GlobalStuff.team2.addPlayer(p);
                                onTeam = true;
                                p.setPlayerListName(org.bukkit.ChatColor.BLUE + "" + p.getName());
                                teamJoined = "Blue";
                                ScoreboardManager.getTeamsMap().put(p, "blue");
                                break;
                            } else {
                                choose = chooseRan(1, 2);
                                break;
                            }
                        } else {
                            choose = chooseRan(1, 2);
                            break;
                        }
                }
            }

            ScoreboardManager.changeLine(p, 15, Main.c(null, "&3» &b&lGame"));
            ScoreboardManager.changeLine(p, 14, Main.c(null, "&rHotPotato"));
            ScoreboardManager.changeLine(p, 13, Main.c(null, " "));
            ScoreboardManager.changeLine(p, 12, Main.c(null, "&3» &b&lPlayers"));
            ScoreboardManager.changeLine(p, 11, Main.c(null, "&r" + Integer.toString(Bukkit.getOnlinePlayers().size()) + "/16"));
            ScoreboardManager.changeLine(p, 10, Main.c(null, "  "));
            ScoreboardManager.changeLine(p, 9, Main.c(null, "&3» &b&lTeam"));
            ScoreboardManager.changeLine(p, 8, Main.c(null, "&r" + teamJoined));
            ScoreboardManager.changeLine(p, 7, Main.c(null, "   "));
            ScoreboardManager.changeLine(p, 6, Main.c(null, "&3» &b&lKit"));
            ScoreboardManager.changeLine(p, 5, Main.c(null, "&r" + ScoreboardManager.getKitsMap().get(p)));
            ScoreboardManager.changeLine(p, 4, Main.c(null, "    "));
            ScoreboardManager.changeLine(p, 3, Main.c(null, "&3» &b&lMap"));
            ScoreboardManager.changeLine(p, 2, Main.c(null, "&r" + GlobalStuff.mapSelected));
        }
    }

    private void registerListeners(Listener... listeners) {
        Arrays.stream(listeners).forEach(listener -> getServer().getPluginManager().registerEvents(listener, this));

        for (TimerEvent.TimerType t : TimerEvent.TimerType.values()) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    getServer().getPluginManager().callEvent(new TimerEvent(t));
                }
            }.runTaskTimer(this, 0, t.getTicks());
        }

    }

    public static FileConfiguration getConfigs() {
        return config;
    }
    public static FileConfiguration getMaps() { return maps; }

    private void loadConfig() {
        if (!getDataFolder().exists()) getDataFolder().mkdir();
        mapsFile = new File(getDataFolder(), "maps.yml");
        configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            configFile.getParentFile().mkdirs();
            copy(getResource("config.yml"), configFile);
        }
        if (!mapsFile.exists()) {
            mapsFile.getParentFile().mkdirs();
            copy(getResource("maps.yml"), mapsFile);
        }
        config = new YamlConfiguration();
        maps = new YamlConfiguration();
        loadYamls();
    }


    public void loadYamls() {
        try {
            config.load(configFile); //loads the contents of the File to its FileConfiguration
            maps.load(mapsFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveYamls() {
        try {
            config.save(configFile); //saves the FileConfiguration to its File
            maps.save(mapsFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void copy(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static Main get() {
        return instance;
    }

}
