package io.github.Block2Block.HotPotato.Managers;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GlobalStuff {

    public static ScoreboardManager sm = Bukkit.getScoreboardManager();
    public static Scoreboard s = sm.getNewScoreboard();

    public static Team team = s.registerNewTeam("red");
    public static Team team2 = s.registerNewTeam("blue");

    public static boolean inGame = false;

    public static boolean isDamageArtificial = false;

    public static List<String> maps = new ArrayList<>();
    private static List<String> coords = new ArrayList<>();
    private static Map<String, World> worlds = new HashMap<>();
    private static Map<String, List<Location>> redcoordsmap = new HashMap<>();
    private static Map<String, List<Location>> bluecoordsmap = new HashMap<>();
    private static Map<String, List<Location>> tntSpawns = new HashMap<>();

    public static int livesRed;
    public static int livesBlue;

    public static String mapSelected = "None";

    public static Map<String, World> getWorldsMap() {
        return worlds;
    }

    public static Map<String, List<Location>> getRedCoordsMap() {
        return redcoordsmap;
    }
    public static Map<String, List<Location>> getBlueCoordsMap() {
        return bluecoordsmap;
    }
    public static Map<String, List<Location>> getTntSpawns() {
        return tntSpawns;
    }

}

