package io.github.Block2Block.HotPotato.Managers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ScoreboardManager {

    private static Map<Player, Objective> objectives = new HashMap<>();
    private static Map<Player, Scoreboard> scoreboards = new HashMap<>();
    private static Map<Player, String> team = new HashMap<>();
    private static Map<Player, String> kit = new HashMap<>();

    public void sendTitle(Player player, String text, String subtitle, int fadeInTime, int showTime, int fadeOutTime, ChatColor color)
    {
        try
        {
            Object chatTitle = getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\": \"" + text + "\",color:" + color.name().toLowerCase() + "}");

            Constructor<?> titleConstructor = getNMSClass("PacketPlayOutTitle").getConstructor(getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], getNMSClass("IChatBaseComponent"), int.class, int.class, int.class);
            Object packet = titleConstructor.newInstance(getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TITLE").get(null), chatTitle, fadeInTime, showTime, fadeOutTime);

            sendPacket(player, packet);
        }

        catch (Exception ex)
        {
            //Do something
        }
    }

    private void sendPacket(Player player, Object packet)
    {
        try
        {
            Object handle = player.getClass().getMethod("getHandle").invoke(player);
            Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
            playerConnection.getClass().getMethod("sendPacket", getNMSClass("Packet")).invoke(playerConnection, packet);
        }
        catch(Exception ex)
        {
            //Do something
        }
    }

    private Class<?> getNMSClass(String name)
    {
        try
        {
            return Class.forName("net.minecraft.server" + Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3] + "." + name);
        }
        catch(ClassNotFoundException ex)
        {
            //Do something
        }
        return null;
    }

    public static void changeLine(Player player, int line, String changeTo) {
        Objective objective  = objectives.get(player);
        Set<String> entries = scoreboards.get(player).getEntries();
        for (String p : entries) {
            if (objective.getScore(p).getScore() == line) {
                scoreboards.get(player).resetScores(p);
            }
        }
        Score score = objective.getScore(changeTo);
        score.setScore(line);
        player.setScoreboard(scoreboards.get(player));
    }

    public static void changeLineGlobal(int line, String changeTo) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            Objective objective  = objectives.get(p);
            Set<String> entries;
            try {
                entries = scoreboards.get(p).getEntries();
            } catch (NullPointerException e) {
                return;
            }
            for (String p2 : entries) {
                if (objective.getScore(p2).getScore() == line) {
                    scoreboards.get(p).resetScores(p2);
                }
            }
            Score score = objective.getScore(changeTo);
            score.setScore(line);
            p.setScoreboard(scoreboards.get(p));
        }
    }

    public static void resetLine(Player player, int line) {
        Objective objective = objectives.get(player);
        Set<String> entries = scoreboards.get(player).getEntries();
        for (String p : entries) {
            if (objective.getScore(p).getScore() == line) {
                scoreboards.get(player).resetScores(p);
            }
        }
        player.setScoreboard(scoreboards.get(player));
    }

    public static void resetLineGlobal(int line) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            Objective objective = objectives.get(p);
            Set<String> entries = scoreboards.get(p).getEntries();
            for (String p2 : entries) {
                if (objective.getScore(p2).getScore() == line) {
                    scoreboards.get(p).resetScores(p2);
                }
            }
            p.setScoreboard(scoreboards.get(p));
        }
    }

    public static Map<Player, Objective> getMap() {
        return objectives;
    }

    public static Map<Player, String> getTeamsMap() {
        return team;
    }

    public static Map<Player, Scoreboard> getScoreboardMap() {
        return scoreboards;
    }

    public static Map<Player, String> getKitsMap() { return kit; }
}
