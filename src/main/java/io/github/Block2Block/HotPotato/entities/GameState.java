package io.github.Block2Block.HotPotato.entities;

public enum GameState {
    INGAME("In-Game", true, 600),
    PREGAME("Starting", true, 10),
    ENDGAME("Ending", false, 10),
    LOBBY("Lobby", false, 20),
    DEAD("Inactive", false, -1);

    String name;
    boolean active;
    int defaultTime;

    GameState(String name, boolean active, int defaultTime) {
        this.active = active;
        this.defaultTime = defaultTime;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isActive() {
        return active;
    }

    public int getDefaultTime() {
        return defaultTime;
    }
}