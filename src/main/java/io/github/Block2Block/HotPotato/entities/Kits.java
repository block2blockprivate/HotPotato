package io.github.Block2Block.HotPotato.entities;

import io.github.Block2Block.HotPotato.Managers.ScoreboardManager;
import org.bukkit.entity.Player;

public enum Kits {
    DEFAULT("Default", 1);

    private String displayName;
    private int id;

    Kits(String displayName, int id) {
        this.displayName = displayName;
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public int getId() {
        return id;
    }

    public static Kits getKit(Player player) {
        for (Kits k : values()) {
            if (Integer.parseInt(ScoreboardManager.getKitsMap().get(player)) == k.getId()) {
                return k;
            }
        }
        return null;
    }
}
