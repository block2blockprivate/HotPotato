package io.github.Block2Block.HotPotato.Commands;

import io.github.Block2Block.HotPotato.Main;
import io.github.Block2Block.HotPotato.entities.Kits;
import io.github.Block2Block.HotPotato.Managers.ScoreboardManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandKit implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (args.length == 1) {
                try {
                    ScoreboardManager.getKitsMap().remove(p);
                    ScoreboardManager.getKitsMap().put(p, args[0]);
                    ScoreboardManager.changeLine(p, 5, Main.c(null, "&r" + Kits.getKit(p).getDisplayName()));
                } catch (Exception e) {
                    sender.sendMessage(Main.c("HotPotato","That kit ID does not exist!"));
                }
            } else {
                sender.sendMessage(Main.c("HotPotato","Incorrect usage. Correct usage: &a/kit <kit ID>"));
            }
        } else {
            sender.sendMessage(Main.c("HotPotato","This command can only be executed from in-game."));
        }
        return false;
    }
}
