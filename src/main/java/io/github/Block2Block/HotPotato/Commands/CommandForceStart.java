package io.github.Block2Block.HotPotato.Commands;

import io.github.Block2Block.HotPotato.Main;
import io.github.Block2Block.HotPotato.Managers.GlobalStuff;
import io.github.Block2Block.HotPotato.Managers.ScoreboardManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandForceStart implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (p.getName().equals("Block2Block")) {
                if (args.length == 1) {
                    if (Bukkit.getOnlinePlayers().size() > 1) {
                        int s;
                        try {
                            s = Integer.parseInt(args[0]) + 1;
                        } catch (NumberFormatException e) {
                            p.sendMessage(Main.c("HotPotato", "Incorrect usage. Correct usage: &a/forcestart <time>"));
                            return false;
                        }
                        if (Main.timerStarted) {
                            if (Main.timerLength > s) {
                                Main.timerLength = s;
                            }
                            Main.timer2.cancel();
                        }
                        Main.timer();
                        for (Player p2 : Bukkit.getOnlinePlayers()) {
                            if (io.github.Block2Block.HotPotato.Managers.ScoreboardManager.getKitsMap().get(p2) == null) {
                                io.github.Block2Block.HotPotato.Managers.ScoreboardManager.getKitsMap().remove(p2);
                                io.github.Block2Block.HotPotato.Managers.ScoreboardManager.getKitsMap().put(p2,"1");
                                io.github.Block2Block.HotPotato.Managers.ScoreboardManager.changeLine(p2,5, Main.c(null, "&rDefault"));
                            }
                        }
                        if (GlobalStuff.mapSelected.equals("None")) {
                            GlobalStuff.mapSelected = GlobalStuff.maps.get(Main.chooseRan(0,GlobalStuff.maps.size()-1));
                            ScoreboardManager.changeLineGlobal(2, Main.c(null, "&r" + GlobalStuff.mapSelected));
                        }
                        return true;
                    } else {
                        sender.sendMessage(Main.c("HotPotato","There aren't enough players online!"));
                    }
                } else if (args.length == 0) {
                    if (!Main.timerStarted) {
                        if (!Main.gameStarted) {
                            if (Bukkit.getOnlinePlayers().size() > 1) {
                                Main.startTimer();
                            } else {
                                sender.sendMessage(Main.c("HotPotato","There aren't enough players online!"));
                            }
                        } else {
                            p.sendMessage(Main.c("HotPotato","The game is already in progress!"));
                            return false;
                        }
                    } else {
                        p.sendMessage(Main.c("HotPotato","The timer has already started!"));
                        return false;
                    }
                } else {
                    p.sendMessage(Main.c("HotPotato","Incorrect usage. Correct usage: &a/forcestart <time>"));
                }
            }
        } else {
            sender.sendMessage(Main.c("HotPotato","This command can only be executed from in-game."));
        }
        return false;
    }
}
